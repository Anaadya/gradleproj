package com.npci.newproj.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.npci.newproj.entity.Users;
import com.npci.newproj.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/getallusers")
	public ResponseEntity<List<Users>> getAllUsers(){
	List<Users> users = null;
	try {
	  users = userService.getAllUsers();
		} catch (Exception ex) {
			ex.getMessage();
		}
		return new ResponseEntity<List<Users>>(users, HttpStatus.OK);
	}
}
