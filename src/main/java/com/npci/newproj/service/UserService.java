package com.npci.newproj.service;

import java.util.List;

import com.npci.newproj.entity.Users;

public interface UserService {

	public List<Users> getAllUsers();
	public Users getUserById(int userId);
	public Users addOrUpdateUser(Users user);
	public Users deleteUser(int userId) throws Exception;
	
	
}
