package com.npci.newproj.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.npci.newproj.entity.Users;
import com.npci.newproj.repository.UserRepository;
import com.npci.newproj.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public List<Users> getAllUsers() {
		// TODO Auto-generated method stub
		return (List<Users>) userRepository.findAll();
	}

	@Override
	public Users getUserById(int userId) {
		// TODO Auto-generated method stub
		return userRepository.findById(userId).orElse(null);
	}

	@Override
	public Users addOrUpdateUser(Users user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public Users deleteUser(int userId) throws Exception {
		// TODO Auto-generated method stub
		
		Users deletedUser = null;
		try {
			deletedUser = userRepository.findById(userId).orElse(null);
			
			if(deletedUser == null) {
			 throw new Exception("User is not available in database");
			}
		} catch (Exception ex) {
			// TODO: handle exception
			throw ex;
		}
		return null;
	}

}
