package com.npci.newproj.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.npci.newproj.entity.Users;

@Repository
public interface UserRepository extends CrudRepository<Users, Integer> {

}
